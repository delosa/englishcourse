from django.views.generic import *
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from .forms import *
from .models import *
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.core.mail import send_mail
from django.db import connection
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.http import JsonResponse
from django.views.generic import CreateView, ListView, UpdateView, DetailView

class ListUsers(ListView):
    model = Users
    template_name = "lista_usuarios.html"

    #@method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ListUsers, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListUsers, self).get_context_data(**kwargs)
        context['users'] = True
        return context

class AddUsers(SuccessMessageMixin, CreateView):
    model = Users
    form_class = AddUsersForm
    template_name = "crear_usuario.html"
    success_url = reverse_lazy('list_users')
    success_msg = "¡El usuario fue creado exitosamente!"

    #@method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AddUsers, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        users = form.instance
        contra = users.first_name[0] + users.phone + users.last_name[
            0]
        users.username = users.email
        users.set_password(contra)
        self.object = form.save()
        messages.success(self.request, "Se ha registrado exitosamente el usuario")
        
        return super(AddUsers, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "Error al registrar el usuario, por favor revise los datos")
        return super(AddUsers, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(AddUsers, self).get_context_data(**kwargs)
        context['modificar'] = False
        context['users'] = True
        return context
