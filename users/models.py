from django.db import models
from django.contrib.auth.models import User

class Course(models.Model):
    name_course = models.CharField(max_length=100, verbose_name="Type of Course")

    def __str__(self):
        return self.name_course

ROLE = (('Administrator','Administrator'),('Student','Student'),('Teacher','Teacher'))
GENDER = (('Female', 'Female'), ('Male', 'Male'))
STATUS = (('Active', 'Active'), ('On Hold', 'On Hold'), ('Suspend', 'Suspend'))

class Users(User):
    phone = models.CharField(max_length=20, verbose_name="Phone", null=True, blank=True)
    gender = models.CharField(max_length=20, verbose_name="Gender", choices=GENDER, null=True, blank=True)
    country = models.CharField(max_length=20, verbose_name="Country", null=True, blank=True)
    address = models.CharField(max_length=100, verbose_name="Address")
    role = models.CharField(max_length=50, verbose_name="Role",choices=ROLE)
    status = models.CharField(max_length=50, verbose_name="Status",choices=STATUS, null=True, blank=True)

    def __str__(self):
        return '%s %s (%s)' % (self.first_name, self.last_name, self.role)
