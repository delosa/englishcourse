from .models import *
from django import forms
from django.contrib.auth.forms import *


class AddUsersForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddUsersForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = 'First Name'
        self.fields['last_name'].label = 'Last Name'
        self.fields['email'].label = 'Email'
        self.fields['phone'].label = 'Phone'
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    class Meta:
        model = Users
        fields = (
            'first_name',
            'last_name',
            'email',
            'phone',
            'country',
            'address',
            'gender',
            'role',
            'status'
        )

        widgets = {
            'phone': forms.NumberInput(attrs={'required': 'true', 'max_length': '12', 'min': '1'}),
            'last_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'first_name': forms.TextInput(attrs={'required': 'true', 'max_length': '100'}),
            'email': forms.EmailInput(),
            'address': forms.TextInput(attrs={'max_length': '100'}),

        }

    def clean(self):
        cleaned_data = super(AddUsersForm, self).clean()
        correo = cleaned_data.get("email")

        try:
            if User.objects.filter(username=correo):
                self._errors['email'] = [
                    'Ya existe un usuario con este correo electrónico']
        except:
            pass
