from django.urls import path
from .views import *
from .forms import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('list_users/', ListUsers.as_view(), name="list_users"),
    path('add_users/', AddUsers.as_view(), name="add_users"),
]