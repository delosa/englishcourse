from django.db import models
from django.core.validators import RegexValidator
import re


re_alpha = re.compile('[^\W\d_]+$', re.UNICODE)
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')
alpha = RegexValidator(re_alpha, 'Solamente se reciben caracteres de A-Z')

