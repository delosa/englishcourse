from django.urls import path
from .views import *
from .forms import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('iniciar_sesion/', auth_views.LoginView.as_view(template_name='./login.html', form_class=IniciarSesionForm), name="iniciar_sesion"),
    path('index/', index, name="index"),
]