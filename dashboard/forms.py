from .models import *
from django import forms
from django.contrib.auth.forms import *
from users.models import *

class IniciarSesionForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(IniciarSesionForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = 'User'
        self.fields['password'].label = 'Password'

    class Meta:
        model = Users

        username = forms.CharField(label="User", max_length=100,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
        password = forms.CharField(label="Password", max_length=100,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))
